SELECT 
    extract(week from s.time_id) AS week_number,
    s.time_id,
    to_char(s.time_id,'Day') AS day_of_week,
    SUM(s.amount_sold) AS total_sales,
    SUM(SUM(s.amount_sold)) OVER (
        PARTITION BY extract(week from s.time_id)  
        ORDER BY extract(isodow from s.time_id)
        RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
    ) AS cumulative_sum,
    CASE
        WHEN extract(isodow from s.time_id) = 1 THEN
            AVG(sum(s.amount_sold)) OVER (ORDER BY s.time_id ROWS BETWEEN 2 PRECEDING AND 1 FOLLOWING)
        WHEN extract(isodow from s.time_id) = 5 THEN
            AVG(sum(s.amount_sold)) OVER (ORDER BY s.time_id ROWS BETWEEN 1 PRECEDING AND 2 FOLLOWING)
        ELSE
            AVG(sum(s.amount_sold)) OVER (ORDER BY s.time_id ROWS BETWEEN 1 PRECEDING AND 1 FOLLOWING)
    END AS centered_3_day_avg
FROM 
    sh.sales s
WHERE 
    extract(year from s.time_id) = 1999 AND 
    extract(week from s.time_id) BETWEEN 49 AND 51
GROUP BY 
    week_number, s.time_id, day_of_week;